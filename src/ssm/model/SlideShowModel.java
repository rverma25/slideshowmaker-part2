package ssm.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import ssm.view.SlideShowMakerView;

/**
 * This class manages all the data associated with a slideshow.
 * 
  * @author McKilla Gorilla & Rahul Verma
 * id: 110134817
 */
public class SlideShowModel {
    SlideShowMakerView ui;
    String title;
    ObservableList<Slide> slides;
    Slide selectedSlide;
    int x;
    
    public SlideShowModel(SlideShowMakerView initUI) {
        x=0;
	ui = initUI;
	slides = FXCollections.observableArrayList();
	reset();	
    }

    // ACCESSOR METHODS
    public boolean isSlideSelected() {
	return selectedSlide != null;
    }
    
    public ObservableList<Slide> getSlides() {
	return slides;
    }
    
    public Slide getSelectedSlide() {
	return selectedSlide;
    }
    
    public void setSlides(ObservableList<Slide> s){
        slides=s;
    }

    public String getTitle() { 
	return title; 
    }
    
    // MUTATOR METHODS
    public void setSelectedSlide(Slide initSelectedSlide) {
	selectedSlide = initSelectedSlide;
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }

    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	slides.clear();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
	selectedSlide = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     */
    public void addSlide(   String initImageFileName,
			   String initImagePath) {
	Slide slideToAdd = new Slide(initImageFileName, initImagePath);
     
	slides.add(slideToAdd);
	selectedSlide = slideToAdd;
	ui.reloadSlideShowPane(this);
    }
    
    public void moveUp(Slide s){
        Slide temp;
        for(int i = 0; i<slides.size(); i++){
            if(slides.get(i)==s){
                if(i>0){
                    temp = slides.get(i-1);
                    slides.set(i-1, s);
                    slides.set(i, temp);
                }
            }
        }
       // System.out.print(slides);
        ui.reloadSlideShowPane(this);
    }
    
    public void moveDown(Slide s){
        Slide temp;
        for(int i = 0; i<slides.size(); i++){
            if(slides.get(i)==s){
                if(i<(slides.size()-1)){
                    temp = slides.get(i+1);
                    slides.set(i+1, s);
                    slides.set(i, temp);
                    break;
                }
            }
        }
       // System.out.print(slides);
        ui.reloadSlideShowPane(this);
        
    }
    
    public void setTitleTextField(){
        ui.setTitleTextField(getTitle());
    }    
    
    public int getX(){
        return x;
    }
   
    public ObservableList<Slide> getNext(){
         ObservableList<Slide> tempSlides;
        tempSlides=FXCollections.observableArrayList(slides);
        
        if(x<slides.size()-1){
         
         x++;
         tempSlides.set(0, slides.get(x));
                   
        }       
        
        
        return tempSlides;
    }
    
    
    public ObservableList<Slide> getPrev(){
         ObservableList<Slide> tempSlides;
        tempSlides=FXCollections.observableArrayList(slides);
      
        if(x>-1){
         
         x--;
         System.out.print(x);

         tempSlides.set(0, slides.get(x));
                 
        }       
        
        
        return tempSlides;
    }
    
}