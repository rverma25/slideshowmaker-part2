package ssm.view;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.TOOLTIP_ADD_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_EXIT;
import static ssm.LanguagePropertyType.TOOLTIP_LOAD_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_DOWN;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_UP;
import static ssm.LanguagePropertyType.TOOLTIP_NEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_NEXT_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_PREVIOUS_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_REMOVE_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_SAVE_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_VIEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.LABEL_CAPTION;
import static ssm.LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_FILE_TOOLBAR;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_SHOW_EDIT_VBOX;
import static ssm.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_WORKSPACE;
import static ssm.StartupConstants.CSS_PRIMARYSCENE;
import static ssm.StartupConstants.CSS_TANDCTEXT;
import static ssm.StartupConstants.CSS_TITLEBOX;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static ssm.StartupConstants.ICON_ADD_SLIDE;
import static ssm.StartupConstants.ICON_REMOVE_SLIDE;
import static ssm.StartupConstants.ICON_EXIT;
import static ssm.StartupConstants.ICON_LOAD_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_MOVE_DOWN;
import static ssm.StartupConstants.ICON_MOVE_UP;
import static ssm.StartupConstants.ICON_NEW_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.ICON_SAVE_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_VIEW_SLIDE_SHOW;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.controller.FileController;
import ssm.controller.SlideShowEditController;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import static ssm.file.SlideShowFileManager.JSON_CAPTION;
import static ssm.file.SlideShowFileManager.JSON_EXT;
import static ssm.file.SlideShowFileManager.JSON_IMAGE_FILE_NAME;
import static ssm.file.SlideShowFileManager.JSON_IMAGE_PATH;
import static ssm.file.SlideShowFileManager.JSON_SLIDES;
import static ssm.file.SlideShowFileManager.JSON_TITLE;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 * This class provides the User Interface for this application,
 * providing controls and the entry points for creating, loading, 
 * saving, editing, and viewing slide shows.
 * 
 *  * @author McKilla Gorilla & Rahul Verma
 * id: 110134817
 */
public class SlideShowMakerView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    HBox fileToolbarPane;
    HBox topToolbarFullView;
    Button newSlideShowButton;
    Button loadSlideShowButton;
    Button saveSlideShowButton;
    Button viewSlideShowButton;
    Button exitButton;
    
    // WORKSPACE
    HBox workspace;
    VBox workingVBox;
    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox slideEditToolbar;
    Button addSlideButton;
    Button removeSlideButton;
    Button moveUpButton;
    Button moveDownButton;
    Button nextButton;
    Button previousButton;
   
    // AND THIS WILL GO IN THE CENTER
    VBox TitleVbox;//////////////////////
    Label TitleLabel;///////////////////////////////////
    TextField TitleTextField;//////////////////////////
    ScrollPane slidesEditorScrollPane;
    VBox slidesEditorPane;
    ////////////////////////////////
    Boolean firstGo;
    int x;
    int y;
    int d;
    /////////////////////////////////////
    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    SlideShowModel slideShow;
   

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    SlideShowFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private SlideShowEditController editController;
    
    public SlideShowMakerView(){
        
    }

    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public SlideShowMakerView(SlideShowFileManager initFileManager) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	
	// MAKE THE DATA MANAGING MODEL
	slideShow = new SlideShowModel(this);
        x = 0;
        y = 0;
        d = 0;
	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	errorHandler = new ErrorHandler(this);
    }

    // ACCESSOR METHODS
    public SlideShowModel getSlideShow() {
	return slideShow;
    }

    public Stage getWindow() {
	return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }

    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

          
	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(windowTitle);
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
	TitleTextField = new TextField();////////////////////
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	slideEditToolbar = new VBox();
        topToolbarFullView = new HBox();
	slideEditToolbar.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
	addSlideButton = this.initChildButton(slideEditToolbar,		ICON_ADD_SLIDE,	   TOOLTIP_ADD_SLIDE,	   CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	removeSlideButton = this.initChildButton(slideEditToolbar,		ICON_REMOVE_SLIDE,	   TOOLTIP_REMOVE_SLIDE,	   CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  false);
        moveUpButton = this.initChildButton(slideEditToolbar,		ICON_MOVE_UP,	   TOOLTIP_MOVE_UP,	   CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  false);
        moveDownButton = this.initChildButton(slideEditToolbar,		ICON_MOVE_DOWN,	   TOOLTIP_MOVE_DOWN,	   CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  false);
        previousButton = this.initChildButton(topToolbarFullView,		ICON_PREVIOUS,	   TOOLTIP_PREVIOUS_SLIDE,	   CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  false);
        nextButton = this.initChildButton(topToolbarFullView,		ICON_NEXT,	   TOOLTIP_NEXT_SLIDE,	   CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  false);
        
	// AND THIS WILL GO IN THE CENTER
	slidesEditorPane = new VBox();
	slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
        
        workingVBox = new VBox();
        
	workingVBox.getChildren().add(slidesEditorScrollPane);

        // NOW PUT THESE TWO IN THE WORKSPACE
        
	workspace.getChildren().add(slideEditToolbar);
	workspace.getChildren().add(workingVBox);
        workspace.getStyleClass().add(CSS_CLASS_WORKSPACE);
    }

    private void initEventHandlers() {
	// FIRST THE FILE CONTROLS
	fileController = new FileController(this, fileManager);
	newSlideShowButton.setOnAction(e -> {
	   fileController.handleNewSlideShowRequest();
	});
	loadSlideShowButton.setOnAction(e -> {
	   fileController.handleLoadSlideShowRequest();
	});
	saveSlideShowButton.setOnAction(e -> {
	   fileController.handleSaveSlideShowRequest();
	});
	exitButton.setOnAction(e -> {
	   fileController.handleExitRequest();
	});
        
        viewSlideShowButton.setOnAction( e -> {
            try {
                viewWithWeb(this.getSlideShow());
            } catch (IOException ex) {
                Logger.getLogger(SlideShowMakerView.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        });
	
	// THEN THE SLIDE SHOW EDIT CONTROLS
	editController = new SlideShowEditController(this);
	addSlideButton.setOnAction(e -> {
	   editController.processAddSlideRequest();
           fileController.markAsEdited();
           saveSlideShowButton.setDisable(false);
	});
        
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
	fileToolbarPane = new HBox();
        fileToolbarPane.getStyleClass().add(CSS_CLASS_SLIDE_FILE_TOOLBAR);
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	newSlideShowButton = initChildButton(fileToolbarPane, ICON_NEW_SLIDE_SHOW,	TOOLTIP_NEW_SLIDE_SHOW,	   CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	loadSlideShowButton = initChildButton(fileToolbarPane, ICON_LOAD_SLIDE_SHOW,	TOOLTIP_LOAD_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	saveSlideShowButton = initChildButton(fileToolbarPane, ICON_SAVE_SLIDE_SHOW,	TOOLTIP_SAVE_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	viewSlideShowButton = initChildButton(fileToolbarPane, ICON_VIEW_SLIDE_SHOW,	TOOLTIP_VIEW_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }

    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	ssmPane = new BorderPane();
	ssmPane.setTop(fileToolbarPane);     
        ssmPane.getStyleClass().add(CSS_PRIMARYSCENE);
	primaryScene = new Scene(ssmPane);
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	   Pane toolbar, 
	   String iconFileName, 
	   LanguagePropertyType tooltip, 
	   String cssClass,
	   boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    
    /**
     * Updates the enabled/disabled status of all toolbar
     * buttons.
     * 
     * @param saved 
     */
    public void updateToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	ssmPane.setCenter(workspace);
	
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	saveSlideShowButton.setDisable(saved);
	viewSlideShowButton.setDisable(false);
	
	// AND THE SLIDESHOW EDIT TOOLBAR
	addSlideButton.setDisable(false);
    }
    public void setTitleTextField(String z){
        TitleTextField.setText(z);
    }
    /**
     * Uses the slide show data to reload all the components for
     * slide editing.
     * 
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadSlideShowPane(SlideShowModel slideShowToLoad) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	slidesEditorPane.getChildren().clear();
         TitleVbox = new VBox();//////////
         TitleVbox.getStyleClass().add(CSS_TITLEBOX);
        slideShowToLoad.setTitleTextField();
        TitleLabel = new Label(props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE));///////////////
        TitleLabel.getStyleClass().add(CSS_TANDCTEXT);
            TitleTextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent key) {
                if(key.getCode().equals(KeyCode.ENTER)){
                    slideShowToLoad.setTitle(TitleTextField.getText());
                     fileController.markAsEdited();
           saveSlideShowButton.setDisable(false);
                }        
            }
        });
        TitleVbox.getChildren().addAll(TitleLabel,TitleTextField);////////////////////
	workingVBox.getChildren().add(TitleVbox);/////////////////////////////
        slidesEditorPane.getChildren().add(TitleVbox);//////////
	for (Slide slide : slideShowToLoad.getSlides()) {
	   SlideEditView slideEditor = new SlideEditView(slide);
           slideEditor.setUI(this);
            slideEditor.setCaption(slide.getCaption());
            	
       
	   slidesEditorPane.getChildren().add(slideEditor);
          
              dimSlidesAndNullStyles();
              slideSelection(slideShowToLoad);
	}
    }
    public void dimSlidesAndNullStyles(){
        boolean t = true;
       for (Node node: slidesEditorPane.getChildren()) {
           if(!t){
           node.setOpacity(.25);
           node.setStyle(null);
           }
           else
               t=false;
           
       }
       moveUpButton.setDisable(true);
       moveDownButton.setDisable(true);
       removeSlideButton.setDisable(true);
    }
    
    public void slideSelection(SlideShowModel slideshow){
        boolean t = true;
        
        for (Node node: slidesEditorPane.getChildren()){
          if(!t){
            SlideEditView s = (SlideEditView)node;  
         
            node.setOnMouseClicked(e->{
            dimSlidesAndNullStyles();
            
            moveUpButton.setDisable(false);
            moveDownButton.setDisable(false);
            removeSlideButton.setDisable(false);
            String styleborder = "-fx-border-color: purple;"
                                + "-fx-border-width: 6;"
                                + "-fx-border-style: dashed;";
            node.setStyle(styleborder);
            node.setOpacity(1);
            
            
                removeSlideButton.setOnAction(k->{
                    slidesEditorPane.getChildren().remove(node);
                    slideShow.getSlides().removeAll(s.slide);
                    fileController.markAsEdited();
                     saveSlideShowButton.setDisable(false);
                });
            
                
                
                Slide x = slideShow.getSlides().get(slideShow.getSlides().size()-1);
                Slide y = slideShow.getSlides().get(0);
                
                
                if(s.getSlide()==y){
                    moveUpButton.setDisable(true);
                }
                else{
                    moveUpButton.setDisable(false);
                    moveUpButton.setOnAction( j -> {
                    SlideShowModel newSlideShow;
                    //System.out.print(slideshow.getSlides());
                    editController = new SlideShowEditController(this);
                    SlideEditView tt = (SlideEditView)node;
                    editController.processMoveUpSlideRequest(tt.getSlide());
                    node.setOpacity(1);
                    node.setStyle(styleborder);
                    fileController.markAsEdited();
                     saveSlideShowButton.setDisable(false);
                    });
                }
                
                
                
                if(s.getSlide()==x){
                    moveDownButton.setDisable(true);
                }else{
                    moveDownButton.setDisable(false);
                moveDownButton.setOnAction( u -> {
                    SlideShowModel newSlideShow;
                   // System.out.println(slideshow.getSlides());
                    editController = new SlideShowEditController(this);
                    SlideEditView ss = (SlideEditView)node;
                    editController.processMoveDownSlideRequest(ss.getSlide());
                    fileController.markAsEdited();
                     saveSlideShowButton.setDisable(false);
                });
                }           
            });
            
          }
          else{
              t = false;
              //set the move up to false;
          }
          
        }      
    }
    public void actionsOnSlide(SlideEditView slide){
         Slide x = slideShow.getSlides().get(slideShow.getSlides().size()-1);
                Slide y = slideShow.getSlides().get(0);
                Node node = (Node)slide;
                    removeSlideButton.setDisable(false);
                  removeSlideButton.setOnAction(k->{
                    slidesEditorPane.getChildren().remove(node);
                    slideShow.getSlides().removeAll(slide.slide);
                    fileController.markAsEdited();
                     saveSlideShowButton.setDisable(false);
                });
                if(slide.getSlide()==y){
                    moveUpButton.setDisable(true);
                }
                else{
                    moveUpButton.setDisable(false);
                    moveUpButton.setOnAction( j -> {
                    SlideShowModel newSlideShow;
                    //System.out.print(slideshow.getSlides());
                    editController = new SlideShowEditController(this);
                    //SlideEditView tt = (SlideEditView)node;
                    editController.processMoveUpSlideRequest(slide.getSlide());
                    //node.setOpacity(1);
                    //node.setStyle(styleborder);
                    fileController.markAsEdited();
                     saveSlideShowButton.setDisable(false);
                    });
                }
                
                
                
                if(slide.getSlide()==x){
                    moveDownButton.setDisable(true);
                }else{
                    moveDownButton.setDisable(false);
                moveDownButton.setOnAction( u -> {
                    SlideShowModel newSlideShow;
                   // System.out.println(slideshow.getSlides());
                    editController = new SlideShowEditController(this);
                   // SlideEditView ss = (SlideEditView)node;
                    editController.processMoveDownSlideRequest(slide.getSlide());
                    fileController.markAsEdited();
                     saveSlideShowButton.setDisable(false);
                });
                }
        
        
    }
    public void initFullView(SlideShowModel slideShow){
        x=0;
        Stage stage = new Stage();
        stage.setTitle("Slide Show Full Screen View");        
        BorderPane bpane = new BorderPane();
       StackPane stack = new StackPane();
       HBox h = new HBox(new Label(slideShow.getTitle()));
       h.setAlignment(Pos.CENTER);
       
       bpane.setTop(h);
       
       bpane.setBottom(topToolbarFullView);
       bpane.setCenter(stack);
         
        Scene scene = new Scene(bpane,700,700);
        rebuildFullView(slideShow.getSlides(),stack);
      // stack.getChildren().get(2).setOpacity(0);
       // stack.getChildren().get(2).toBack();
       // stack.getChildren().get(1).toFront();
        previousButton.setOnAction(e->{
       /* y++;
         for(Node node: stack.getChildren()){
            node.setOpacity(0);
        }
         if(y<slideShow.getSlides().size()){
         stack.getChildren().get(y).setOpacity(1);  
         }
         else{
             y--;
         }*/
            nextButton.setDisable(false);
            if(slideShow.getX()>-1){
          rebuildFullView(slideShow.getPrev(),stack);   
            }
            if(slideShow.getX()<=0){
                previousButton.setDisable(true);
            }
            
        });
        
        
        nextButton.setOnAction(e->{
            previousButton.setDisable(false);
            if(slideShow.getX()<slideShow.getSlides().size()-1){
            rebuildFullView(slideShow.getNext(),stack);
            }
            if(slideShow.getX()>=slideShow.getSlides().size()-1){
                nextButton.setDisable(true);
            }
  /*      y--;
        for(Node node: stack.getChildren()){
            node.setOpacity(0);
        }
        if(y>=0){
        stack.getChildren().get(y).setOpacity(1);  
        }
        else{
            y++;
        }
        */
  
        });
        stage.setScene(scene);
        stage.show();
        
    }
    
    public void rebuildFullView(ObservableList<Slide> slides, StackPane stack){
          HBox centerScene;
        ImageView im;
        Label l;
        
        y=slides.size()-1;
        
        ObservableList<Slide> tempSlides;
        tempSlides=FXCollections.observableArrayList(slides);
        
        for(int i = tempSlides.size()-1;i>=0;i--){
            try {
                centerScene = new HBox();
                im = new ImageView();
                String imagePath = tempSlides.get(i).getImagePath() + SLASH + tempSlides.get(i).getImageFileName();
                File file = new File(imagePath);
                // GET AND SET THE IMAGE
                URL fileURL = file.toURI().toURL();
                Image slideImage = new Image(fileURL.toExternalForm());
                im.setImage(slideImage);
                l = new Label();
                l.setText(tempSlides.get(i).getCaption());
                // AND RESIZE IT
                im.setFitWidth(600);
                im.setFitHeight(600);
                centerScene.setBackground(new Background(new BackgroundFill(Color.YELLOW,null,null)));
                centerScene.getChildren().addAll(im,l);
                centerScene.setAlignment(Pos.CENTER);
                centerScene.toFront();
                stack.getChildren().add(centerScene);
            } catch (MalformedURLException ex) {
                Logger.getLogger(SlideShowMakerView.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        
    }
    
    
    
    
    
   public void s(){
     fileController.markAsEdited();
           saveSlideShowButton.setDisable(false);
   }
   
   public void viewWithWeb(SlideShowModel model) throws IOException{
        
          
        String jsonFilePath = "./src/webfiles/jsond/slidesjson.json";
        
        // INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);  
           
        // BUILD THE SLIDES ARRAY
        JsonArray slidesJsonArray = makeSlidesJsonArray(model.getSlides());
      
        // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
        JsonObject courseJsonObject = Json.createObjectBuilder()
                                    .add(JSON_TITLE, model.getTitle())
                                    .add(JSON_SLIDES, slidesJsonArray)
                .build();
        
        // AND SAVE EVERYTHING AT ONCE
        jsonWriter.writeObject(courseJsonObject);
       
       
       
       
       
       
       String title = model.getTitle();
       new File("./sites/"+title).mkdir();
       Path source = Paths.get("./src/webfiles/index.html");
       Path target = Paths.get("./sites/" + title+ "/index.html");
       Files.copy(source, target,StandardCopyOption.REPLACE_EXISTING);
       
       new File("./sites/"+title+"/css").mkdir();
       source = Paths.get("./src/webfiles/css/slideshow_style.css");
       target = Paths.get("./sites/" + title+ "/css/slideshow_style.css");
       Files.copy(source, target,StandardCopyOption.REPLACE_EXISTING);     
       
       new File("./sites/"+title+"/js").mkdir();
       source = Paths.get("./src/webfiles/js/SlideShow.js");
       target = Paths.get("./sites/" + title+ "/js/SlideShow.js");
       Files.copy(source, target,StandardCopyOption.REPLACE_EXISTING);     
       
       new File("./sites/"+title+"/jsond").mkdir();
       source = Paths.get("./src/webfiles/jsond/slidesjson.json");
       target = Paths.get("./sites/" + title+ "/jsond/slidesjson.json");
       Files.copy(source, target,StandardCopyOption.REPLACE_EXISTING);    
           
       File file2 = new File("./sites/"+title+"/img");
       file2.mkdir();
       File[] files = file2.listFiles();
       for(File file: files){
           file.delete();
       }
       source = Paths.get("./images/icons/Play.png");
       target = Paths.get("./sites/"+title+"/img/Play.png");
       Files.copy(source, target,StandardCopyOption.REPLACE_EXISTING);    
       source = Paths.get("./images/icons/Pause.png");
       target = Paths.get("./sites/"+title+"/img/Pause.png");
       Files.copy(source, target,StandardCopyOption.REPLACE_EXISTING);    
       source = Paths.get("./images/icons/Previous.png");
       target = Paths.get("./sites/"+title+"/img/Previous.png");
       Files.copy(source, target,StandardCopyOption.REPLACE_EXISTING);    
       source = Paths.get("./images/icons/Next.png");
       target = Paths.get("./sites/"+title+"/img/Next.png");
       Files.copy(source, target,StandardCopyOption.REPLACE_EXISTING);    
       
       for(Slide slide: model.getSlides()){
       source = Paths.get("./images/slide_show_images/"+slide.getImageFileName());
       target = Paths.get("./sites/"+title+"/img/"+slide.getImageFileName());
       Files.copy(source, target,StandardCopyOption.REPLACE_EXISTING);           
       }
       
       
       
       File f = new File("./sites/"+title+"/index.html");
       
       String directPath = f.getAbsolutePath().toString().replace("./","");
       WebView slideView = new WebView();   
       
       slideView.getEngine().load("file:///" + directPath); 
       
       Stage stage = new Stage();
       Scene scene = new Scene(slideView, 800,800);   
       stage.setScene(scene);   
       stage.show();   
      
       
       

   }
   private JsonArray makeSlidesJsonArray(List<Slide> slides) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Slide slide : slides) {
	   JsonObject jso = makeSlideJsonObject(slide);
	   jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;        
    }
   
    private JsonObject makeSlideJsonObject(Slide slide) {
        if(slide.getCaption()!=null){
        JsonObject jso = Json.createObjectBuilder()
		.add(JSON_IMAGE_FILE_NAME, slide.getImageFileName())
		.add(JSON_IMAGE_PATH, slide.getImagePath())
                .add(JSON_CAPTION, slide.getCaption())
		.build();
        return jso;
        }
        else{
             JsonObject jso = Json.createObjectBuilder()
		.add(JSON_IMAGE_FILE_NAME, slide.getImageFileName())
		.add(JSON_IMAGE_PATH, slide.getImagePath())
                .add(JSON_CAPTION, "")
		.build();
             return jso;
        }	
    }
   
    
    
    
}