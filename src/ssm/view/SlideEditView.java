package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.CSS_SELECTIONBORDER;
import static ssm.StartupConstants.CSS_TANDCTEXT;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import ssm.controller.ImageSelectionController;
import ssm.error.ErrorHandler;
import ssm.model.Slide;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption.
 * 
 * * @author McKilla Gorilla & Rahul Verma
 * id: 110134817
 */
public class SlideEditView extends HBox {
    
    SlideShowMakerView ui;
    // SLIDE THIS COMPONENT EDITS
    Slide slide;
    
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    Label captionLabel;
    TextField captionTextField;
    
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ImageSelectionController imageController;
    
    

    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initSlide The slide to be edited by this component.
     */
    public SlideEditView(Slide initSlide) {
        
        ui = new SlideShowMakerView();
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
	
	// KEEP THE SLIDE FOR LATER
	slide = initSlide;
	
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
	updateSlideImage();

	// SETUP THE CAPTION CONTROLS
	captionVBox = new VBox();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	captionLabel = new Label(props.getProperty(LanguagePropertyType.LABEL_CAPTION));
        captionLabel.getStyleClass().add(CSS_TANDCTEXT);
	captionTextField = new TextField();
	captionVBox.getChildren().add(captionLabel);
	captionVBox.getChildren().add(captionTextField);

	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(imageSelectionView);
	getChildren().add(captionVBox);

	// SETUP THE EVENT HANDLERS
	imageController = new ImageSelectionController();
       
	imageSelectionView.setOnMousePressed(e -> {
            ui.dimSlidesAndNullStyles();
            ui.actionsOnSlide(this);
	   imageController.processSelectImage(slide, this);
           this.setOpacity(1);
           
           this.getStyleClass().add(CSS_SELECTIONBORDER);
	});
        captionTextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent key) {
                if(key.getCode().equals(KeyCode.ENTER)){
                    slide.setCaption(captionTextField.getText());
                    ui.s();
                }        
            }
        });
    }
    
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    public void updateSlideImage() {
	String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
	
        File file2 = new File(slide.getImagePath());
        File[] files = file2.listFiles();
        boolean exists = false;
         for(File child: files){
               if(slide.getImageFileName().equalsIgnoreCase(child.getName())){
                   exists=true;
               }
               
           }
           if(!exists){
               imagePath = slide.getImagePath() + SLASH + "DefaultStartSlide.png";               
           }
           File file = new File(imagePath);           
	try {
	   // GET AND SET THE IMAGE
	   URL fileURL = file.toURI().toURL();
           
	   Image slideImage = new Image(fileURL.toExternalForm());
          
	   imageSelectionView.setImage(slideImage);
	   
	   // AND RESIZE IT
	   double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
	   double perc = scaledWidth / slideImage.getWidth();
	   double scaledHeight = slideImage.getHeight() * perc;
	   imageSelectionView.setFitWidth(scaledWidth);
	   imageSelectionView.setFitHeight(scaledHeight);
           
	} catch (Exception e) {
	   ErrorHandler eH = ui.getErrorHandler();
                PropertiesManager props = PropertiesManager.getPropertiesManager();
            eH.processError(LanguagePropertyType.ERROR_MISSING_IMAGE, props.getProperty(LanguagePropertyType.ERROR_MISSING_IMAGE), props.getProperty(LanguagePropertyType.ERROR_MISSING_IMAGE_MESSAGE));
	}
    }
    
    public void setCaption(String cap){
        this.captionTextField.setText(cap);
    }
    public ImageView getImageSelectionView(){
        return imageSelectionView;
    }
    
    public Slide getSlide(){
        return slide;
    }
    
    public void setUI(SlideShowMakerView i){
        ui=i;
        imageController.setUI(ui);
    }
    
    public SlideShowMakerView getUI(){
        return ui;
    }
}